#ifndef DATA_H
#define DATA_H

#include "datatypes.h"


// ============== Global Variables ===============
BYTE	pFrameL[FRAME_WIDE * FRAME_HIGH * 3];
BYTE	pFrameR[FRAME_WIDE * FRAME_HIGH * 3];
int		shade = 0;
int		stereo = 0;
int		eyes = 10;
int     distance = 500;

// ========= Polygon VJS Mesh Variables ==========
POLYGON_3D Pyramid;
POLYGON_3D Icosahedron;

// ================== Game Data ==================
bool paused = false;
bool muted = false;

Player player1;
Vector2 stars[NumStars];

Bullet bullets[MaxNumBullets];
int MaxBulletCount = 3;
int bulletSpeed = 10;
int bulletCount = 0;
bool bulletsSpawned[MaxNumBullets] = {false};

Meteor meteors[MaxNumMeteors];
bool meteorsSpawned[MaxNumMeteors] = {false};
int meteorCount = 0;
int MaxMeteorCount = 1;

Meteor megaMeteor;
bool megaMeteorSpawned = false;



// ============================================
//                Math Functions
// ============================================
float CrossProduct2D (Vector2 v1, Vector2 v2)
{
    return v1.x * v2.y - v1.y * v2.x;
}

float FaceNormal (Vector2 *varr)
{
    Vector2 v1, v2;
    
    v1.x = varr[0].x - varr[1].x;
    v1.y = varr[0].y - varr[1].y;
    
    v2.x = varr[2].x - varr[1].x;
    v2.y = varr[2].y - varr[1].y;
    
    return CrossProduct2D (v1, v2);
}

// =================================================
//          General Transformations Fucntions
// =================================================
void ApplyScaleAndPosition (Vector3 &point, float scale, Vector3 position)
{
    point.x = point.x * scale + position.x;
    point.y = point.y * scale + position.y;
    point.z = point.z * scale + position.z;
}

void ApplyPerspective (Vector3 &point, Vector2 &new_point)
{
    new_point.x = (distance * point.x / (point.z + distance)) + (FRAME_WIDE / 2);
    new_point.y = (distance * point.y / (point.z + distance)) + (FRAME_HIGH / 2);
}

void UpdateAngle(int &angle)
{
    angle = (angle - 2) % 360;
}


// =================================================
//                 Setup Players
// =================================================
void SetupPlayer1()
{
    player1.position.x = 0;
    player1.position.y = -220;
    player1.position.z = 0;
    player1.scale = 0.25;
    player1.lives = 3;
    player1.score = 0;
}

void SetupStars()
{
    for (int i = 0; i < NumStars; i++) {
        stars[i].x = rand() % (FRAME_WIDE-15) + 10;
        stars[i].y = rand() % (FRAME_HIGH-50) + 50;
    }
}


// =================================================
//                 Pyramid Functions
// =================================================
// =========== Load 4 Sided Pyramid Mesh ===========
void LoadPyramidMesh()
{
    int n, j = 0;
    char line[80];
    
    //-- Open & Check File
    FILE *fr;
    fr = fopen ("Assets/Pyramid.vjs", "rt");
    if (!fr) return ;
    
    int r, g, b;
    
    //-- Get The Number of Vertices & Polygons In The File
    fgets(line, 80, fr);
    sscanf(line,"%d %d", &Pyramid.number_of_verts, &Pyramid.number_of_polys);
    
    fgets(line, 80, fr);
    
    //-- Read Each Vertices
    for (int i = 0; i < Pyramid.number_of_verts; i++)
    {
        fgets(line, 80, fr);
        sscanf(line,"%d %d %d %d %d %d %d",&j, &Pyramid.verts[i].vert.x,
               &Pyramid.verts[i].vert.y, &Pyramid.verts[i].vert.z,
               &r, &g, &b);
        
        Pyramid.verts[i].color = (((int)r & 0xff) << 16) + (((int)g & 0xff) << 8) + ((int)b & 0xff);
    }
    
    fgets(line, 80, fr);
    
    //-- Read Each Polygon
    int polyVerts = 0;
    
    for (int i = 0; i < Pyramid.number_of_polys; i++)
    {
        j = 0;
        polyVerts = 0;
        
        //-- Get Number Of Verts In Polygon
        fgets(line, 80, fr);
        sscanf(line,"%d %d", &j, &polyVerts);
        
        if (polyVerts < 3) return;
        
        int pv_index = 0;
        int val;
        
        char *p = line;
        p+=3;
        while (*p) //- While there are more characters
        {
            if (isdigit(*p))
            {
                val = strtol(p, &p, 10); //- read number
                Pyramid.polys[i][pv_index] = val;
                pv_index++;
            }
            else
            {
                p++;
            }
        }
    }
    
    fclose(fr);
}

// ========= Pyramid Rotation ==========
void ApplyPyramidRotation (Vector3 &point, int axis, int degree, int polyindex, int vertIndex)
{
    //-- Rotation data
    double val = PI / 180.0;
    double ret = cos( degree * val );
    double angle = degree * val;
    
    float s = sin(angle);
    float c = cos(angle);
    
    int i = polyindex;
    int k = vertIndex;
    
    //-- X axis
    if (axis == 0) {
        point.x = Pyramid.verts[Pyramid.polys[i][k]].vert.x;
        
        point.y = (Pyramid.verts[Pyramid.polys[i][k]].vert.y * c -
                   Pyramid.verts[Pyramid.polys[i][k]].vert.z * s);
        
        point.z = (Pyramid.verts[Pyramid.polys[i][k]].vert.y * s +
                   Pyramid.verts[Pyramid.polys[i][k]].vert.z * c);
    }
    //-- Y axis
    else if (axis == 1) {
        point.y = Pyramid.verts[Pyramid.polys[i][k]].vert.y;
        
        point.z = (Pyramid.verts[Pyramid.polys[i][k]].vert.z * c -
                   Pyramid.verts[Pyramid.polys[i][k]].vert.x * s);
        
        point.x = (Pyramid.verts[Pyramid.polys[i][k]].vert.z * s +
                   Pyramid.verts[Pyramid.polys[i][k]].vert.x * c);
    }
    //-- Z axis
    else if (axis == 2) {
        point.z = Pyramid.verts[Pyramid.polys[i][k]].vert.z;
        
        point.x = (Pyramid.verts[Pyramid.polys[i][k]].vert.x * c -
                   Pyramid.verts[Pyramid.polys[i][k]].vert.y * s);
        
        point.y = (Pyramid.verts[Pyramid.polys[i][k]].vert.x * s +
                   Pyramid.verts[Pyramid.polys[i][k]].vert.y * c);
    }
    //-- Dont rotate
    else
        return;
}

// ========= Draw Player Ship ==========
void Draw_player(BYTE *screen, int view)
{
    //-- Print Each Polygon
    int numPolys = Pyramid.number_of_polys;
    int polyVerts = Pyramid.number_of_verts;
    
    for (int i = 0; i < numPolys; i++)
    {
        if (polyVerts < 3) return;
        
        int colors[polyVerts];
        int k = 0;
        
        //-- Calculate Rotation
        Vector3 varr[polyVerts];
        Vector2 perspective_varr[100];
        for (k = 0; k < 3; k++)
        {
            colors[k] = Pyramid.verts[Pyramid.polys[i][k]].color;
            
            ApplyPyramidRotation (varr[k], 1, player1.angle, i, k);
            ApplyScaleAndPosition (varr[k], player1.scale, player1.position);
            ApplyPerspective (varr[k], perspective_varr[k]);
        }
        
        //-- Back Face Culling (if normal is negative, dont draw)
        float cp = FaceNormal (perspective_varr);
        if ( cp <= 0 ) continue;
        
        ConvexClipPolygon (screen, view, perspective_varr, 3, colors);
    }
}

// ========= Draw Bullet ==========
void Draw_bullet(BYTE *screen, int view, Bullet &bullet)
{
    //-- Print Each Polygon
    int numPolys = Pyramid.number_of_polys;
    int polyVerts = Pyramid.number_of_verts;
    
    for (int i = 0; i < numPolys; i++)
    {
        if (polyVerts < 3) return;
        
        int colors[polyVerts];
        int k = 0;
        
        //-- Calculate Rotation
        Vector3 varr[polyVerts];
        Vector2 perspective_varr[100];
        for (k = 0; k < 3; k++)
        {
            colors[k] = 0xffffff;
            
            ApplyPyramidRotation (varr[k], 1, bullet.angle, i, k);
            ApplyScaleAndPosition (varr[k], bullet.scale, bullet.position);
            ApplyPerspective (varr[k], perspective_varr[k]);
        }
        
        //-- Back Face Culling (if normal is negative, dont draw)
        float cp = FaceNormal (perspective_varr);
        if ( cp <= 0 ) continue;
        
        ConvexClipPolygon (screen, view, perspective_varr, 3, colors);
    }
}



// =============================================
//             Icosahedron Functions
// =============================================
// =========== Load Icosahedron Mesh ===========
void LoadIcosahedronMesh()
{
    int n, j = 0;
    char line[80];
    
    //-- Open & Check File
    FILE *fr;
    fr = fopen ("Assets/Icosahedron.vjs", "rt");
    if (!fr) return ;
    
    int r, g, b;
    
    //-- Get The Number of Vertices & Polygons In The File
    fgets(line, 80, fr);
    sscanf(line,"%d %d", &Icosahedron.number_of_verts, &Icosahedron.number_of_polys);
    
    fgets(line, 80, fr);
    
    //-- Read Each Vertices
    for (int i = 0; i < Icosahedron.number_of_verts; i++)
    {
        fgets(line, 80, fr);
        sscanf(line,"%d %d %d %d %d %d %d",&j, &Icosahedron.verts[i].vert.x,
               &Icosahedron.verts[i].vert.y, &Icosahedron.verts[i].vert.z,
               &r, &g, &b);
        
        Icosahedron.verts[i].color = (((int)r & 0xff) << 16) + (((int)g & 0xff) << 8) + ((int)b & 0xff);
    }
    
    fgets(line, 80, fr);
    
    //-- Read Each Polygon
    int polyVerts = 0;
    
    for (int i = 0; i < Icosahedron.number_of_polys; i++)
    {
        j = 0;
        polyVerts = 0;
        
        //-- Get Number Of Verts In Polygon
        fgets(line, 80, fr);
        sscanf(line,"%d %d", &j, &polyVerts);
        
        if (polyVerts < 3) return;
        
        int pv_index = 0;
        int val;
        
        char *p = line;
        p+=3;
        while (*p) //- While there are more characters
        {
            if (isdigit(*p))
            {
                val = strtol(p, &p, 10); //- read number
                Icosahedron.polys[i][pv_index] = val;
                pv_index++;
            }
            else
            {
                p++;
            }
        }
    }
    
    fclose(fr);
}

// ========= Icosahedron Rotation ==========
void ApplyIcosaRotation (Vector3 &point, int axis, int degree, int polyindex, int vertIndex)
{
    //-- Rotation data
    double val = PI / 180.0;
    double ret = cos( degree * val );
    double angle = degree * val;
    
    float s = sin(angle);
    float c = cos(angle);
    
    int i = polyindex;
    int k = vertIndex;
    
    //-- X axis
    if (axis == 0) {
        point.x = Icosahedron.verts[Icosahedron.polys[i][k+1]].vert.x;
        
        point.y = (Icosahedron.verts[Icosahedron.polys[i][k+1]].vert.y * c -
                   Icosahedron.verts[Icosahedron.polys[i][k+1]].vert.z * s);
        
        point.z = (Icosahedron.verts[Icosahedron.polys[i][k+1]].vert.y * s +
                   Icosahedron.verts[Icosahedron.polys[i][k+1]].vert.z * c);
    }
    //-- Y axis
    else if (axis == 1) {
        point.y = Icosahedron.verts[Icosahedron.polys[i][k+1]].vert.y;
        
        point.z = (Icosahedron.verts[Icosahedron.polys[i][k+1]].vert.z * c -
                   Icosahedron.verts[Icosahedron.polys[i][k+1]].vert.x * s);
        
        point.x = (Icosahedron.verts[Icosahedron.polys[i][k+1]].vert.z * s +
                   Icosahedron.verts[Icosahedron.polys[i][k+1]].vert.x * c);
    }
    //-- Z axis
    else if (axis == 2) {
        point.z = Icosahedron.verts[Icosahedron.polys[i][k+1]].vert.z;
        
        point.x = (Icosahedron.verts[Icosahedron.polys[i][k+1]].vert.x * c -
                   Icosahedron.verts[Icosahedron.polys[i][k+1]].vert.y * s);
        
        point.y = (Icosahedron.verts[Icosahedron.polys[i][k+1]].vert.x * s +
                   Icosahedron.verts[Icosahedron.polys[i][k+1]].vert.y * c);
    }
    //-- Dont rotate
    else
        return;
}

// ========= Icosahedron Drawing Function ==========
void Draw_icosahedron(BYTE *screen, int view, Vector3 position, float scale, int rotation_degree, int axis)
{
    int numPolys = Icosahedron.number_of_polys;
    int polyVerts = Icosahedron.number_of_verts;
    
    //-- Print Each Polygon
    for (int i = 0; i < numPolys; i++)
    {
        if (polyVerts < 3) return;
        
        int colors[polyVerts];
        Vector3 varr[polyVerts];
        Vector2 perspective_varr[polyVerts];
        
        //-- Apply Transformations
        for (int k = 0; k < 3; k++)
        {
            colors[k] = Icosahedron.verts[Icosahedron.polys[i][k+1]].color;
            
            ApplyIcosaRotation (varr[k], axis, rotation_degree, i, k);
            ApplyScaleAndPosition (varr[k], scale, position);
            ApplyPerspective (varr[k], perspective_varr[k]);
        }
        
        //-- Back Face Culling (if normal is negative, dont draw)
        float cp = FaceNormal (perspective_varr);
        if ( cp <= 0 ) continue;
        
        ConvexClipPolygon (screen, view, perspective_varr, 3, colors[0]);
    }
}


#endif // DATA_H