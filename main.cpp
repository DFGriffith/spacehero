
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdio.h> 

#include <cstdint>
#include <list>
#include <vector>
#include <algorithm>

#include "game.h"
#include "controls.h"

#ifdef _WIN32
#include "libs/glut.h"
#include <windows.h>
#pragma comment(lib, "winmm.lib")			//- not required but have it in anyway
#pragma comment(lib, "libs/glut32.lib")
#elif __APPLE__
#include <GLUT/glut.h>
#include <unistd.h>
#elif __unix__		// all unices including  __linux__
#include <GL/glut.h>
#endif


// ======== Forward Declarations ========
void ClearScreen();
void DrawFrame();
void Interlace(BYTE* pL, BYTE* pR);
void BuildFrame(BYTE *pFrame, int view);
void OnIdle(void);
void OnDisplay(void);
void reshape(int w, int h);
void OnMouse(int button, int state, int x, int y);
void OnKeypress(unsigned char key, int x, int y);
void SpecialInput(int key, int x, int y);
void RenderGUI();

// ==============================================
//              Program Entry Point
// ==============================================
int main(int argc, char** argv)
{
    //-- Load Objects VJS Mesh
    LoadPyramidMesh();
    LoadIcosahedronMesh();
    SetupPlayer1();
    SetupStars();
    
    //-- setup GLUT --
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);	//GLUT_3_2_CORE_PROFILE |
    glutInitWindowSize(FRAME_WIDE, FRAME_HIGH);
    glutInitWindowPosition(100, 100);
    glutCreateWindow(argv[0]);
    
    //--- set openGL state --
    glClearColor (0.0, 0.0, 0.0, 0.0);
    glShadeModel(GL_FLAT);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    
    //-- register call back functions --
    glutIdleFunc(OnIdle);
    glutDisplayFunc(OnDisplay);
    glutReshapeFunc(reshape);
    glutKeyboardFunc(OnKeypress);
    glutSpecialFunc(SpecialInput);
    glutMouseFunc(OnMouse);
    
    //-- run the program
    glutMainLoop();
    return 0;
}



// ==============================================
//                 Event Handers
// ==============================================
void OnIdle(void)
{
    DrawFrame();
    glutPostRedisplay();
}

void OnDisplay(void)
{
    glClear(GL_COLOR_BUFFER_BIT);
    glRasterPos2i(0, 0);
    glDrawPixels(FRAME_WIDE, FRAME_HIGH, GL_RGB,GL_UNSIGNED_BYTE, (GLubyte*)pFrameR);
    
    RenderGUI();
    
    glutSwapBuffers();
    glFlush();
}

void reshape(int w, int h)
{
    glViewport(0, 0, (GLsizei) w, (GLsizei) h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0.0, (GLdouble) w, 0.0, (GLdouble) h);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void OnMouse(int button, int state, int x, int y)
{
    if (button == GLUT_LEFT_BUTTON && state == GLUT_UP)
    {
        PlaySoundEffect("Laser.wav");
        if (++shade > 16) shade = 0;
    }
}


void SpecialInput(int key, int x, int y)
{
    switch(key)
    {
        case GLUT_KEY_UP:
            UpdatePlayerPositionUD(50);
            break;
        case GLUT_KEY_DOWN:
            UpdatePlayerPositionUD(50);
            break;
        case GLUT_KEY_LEFT:
            UpdatePlayerPosition(-100);
            break;
        case GLUT_KEY_RIGHT:
            UpdatePlayerPosition(100);
            break;
    }
    
    glutPostRedisplay();
}

void OnKeypress(unsigned char key, int x, int y)
{
    switch (key)
    {
        //-- Move Player
        case 'a': UpdatePlayerPosition(-100); break;
        case 'd': UpdatePlayerPosition(100); break;
        case 'w': UpdatePlayerPositionUD(50); break;
        case 's': UpdatePlayerPositionUD(-50); break;
        //-- Shoot
        case ' ': SpawnBullet(); break;
        //-- Game Controls
        case 'p': GamePause(); break;
        case 'n': GameReset(); break;
        case 'm': GameMute(); break;
        //-- 3D Controls
        case '3': stereo ^= 1, eyes = 10;break;
        case ']': eyes++;	break;
        case '[': eyes--;	break;
        //-- esc Commeand
        case 27 : exit(0);
    }
    
    glutPostRedisplay();
}



// ==============================================
//               Utility Functions
// ==============================================
void ClearScreen()
{
    memset(pFrameL, 0, FRAME_WIDE * FRAME_HIGH * 3);
    memset(pFrameR, 0, FRAME_WIDE * FRAME_HIGH * 3);
}

void Interlace(BYTE* pL, BYTE* pR)
{
    int rowlen = 3 * FRAME_WIDE;
    for (int y = 0; y < FRAME_HIGH; y+=2)
    {
        for (int x = 0; x < rowlen; x++) *pR++ = *pL++;
        pL += rowlen;
        pR += rowlen;
    }
}

void DrawFrame()
{
    ClearScreen();
    
    if (!stereo) BuildFrame(pFrameR, 0);
    else {
        BuildFrame(pFrameL, -eyes);
        BuildFrame(pFrameR, +eyes);
        Interlace((BYTE*)pFrameL, (BYTE*)pFrameR);
    }
}





// ==============================================
//              Build Frame Function
// ==============================================
void BuildFrame (BYTE *pFrame, int view)
{
    BYTE* screen = (BYTE*)pFrame;	// use copy of screen pointer for safety
    
    GameUpdate(screen, view);
}

void RenderText(int x, int y, float r, float g, float b, void *font, char *string)
{
    glColor3f(1.0f, 1.0f, 1.0f);
    glRasterPos2f(x, y);
    int len, i;
    len = (int)strlen(string);
    
    for (i = 0; i < len; i++) {
        glutBitmapCharacter(font, string[i]);
    }
    
}

void RenderGUI()
{
    char buf[100];
    sprintf(buf, "Score : %d", player1.score);
    RenderText(10, 10, 255, 255, 255, GLUT_BITMAP_8_BY_13, buf);
    sprintf(buf, "Lives : %d", player1.lives);
    RenderText(10, 25, 255, 255, 255, GLUT_BITMAP_8_BY_13, buf);
    
    if (player1.lives <= 0) {
        sprintf(buf, "GAME OVER");
        RenderText(185, 300, 255, 255, 255, GLUT_BITMAP_TIMES_ROMAN_24, buf);
        sprintf(buf, "Press 'N' to play again");
        RenderText(170, 270, 255, 255, 255, GLUT_BITMAP_8_BY_13, buf);
    }
    else if (paused) {
        sprintf(buf, "PAUSED");
        RenderText(200, 300, 255, 255, 255, GLUT_BITMAP_TIMES_ROMAN_24, buf);
    }
}

