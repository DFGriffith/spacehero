#ifndef DATATYPES_H
#define DATATYPES_H


//====== Macros and Defines =========
#define FRAME_WIDE	500
#define FRAME_HIGH	600

#define PI 3.14159265

#define MaxNumPts 100   //- max # pts in object
#define MaxNumPolys 100 //- max # polygons

#define MaxNumMeteors 5
#define MaxNumBullets 3
#define NumStars 50

#define MeteorScaleSmall 0.2  //- # pts for all polys
#define MeteorRangeSmall 40
#define MeteorScaleLarge 0.5
#define MeteorRangeLarge 80


//====== Structs & typedefs =========
typedef unsigned char BYTE;

typedef struct POINT2D {int x, y;} Vector2;

typedef struct POINT2DF {float x, y;} Vector2f;

typedef struct POINT3D
{
    int x,y,z;
}Vector3;


typedef struct
{
    int xL, xR;
    int cL, cR;
} EDGE_LIST [FRAME_HIGH];


typedef struct
{
    Vector3 vert;
    int color;
} POLY_VERT3;

typedef struct
{
    int number_of_verts;
    int number_of_polys;
    POLY_VERT3 verts[MaxNumPts];
    int polys[MaxNumPolys][MaxNumPts];
} POLYGON_3D;

// ======== Meteor + Bullet Object ========
typedef struct
{
    Vector3 position;
    int angle;
    float scale;
    int speed;
    int axis;
} Meteor, Bullet;

// ======== Player Object ========
typedef struct
{
    Vector3 position;
    int angle;
    float scale;
    int lives;
    int score;
} Player;

#endif // DATATYPES_H