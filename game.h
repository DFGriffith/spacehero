#ifndef GAME_H
#define GAME_H

#include "drawing.h"
#include "datatypes.h"
#include "data.h"


// ======== Forward Declarations ========
void UpdateObjects ();
void DrawObjects (BYTE *screen, int view);
void DrawStars (BYTE *screen, int view);
void PlaySoundEffect(char * filename);

void SpawnSmallMeteor ();
void SpawnMegaMeteor();
void UpdateMeteorPosition(Meteor &meteor, int meteorNumber);
void DestroyMeteor (int meteorNumber);

void UpdateBullet (Bullet &bullet, int bulletNumber);
void RemoveBullet (Bullet &bullet, int bulletNumber);
bool MeteorInRange(Bullet &bullet, int bulletNumber, int meteorNumber);


// ============================================
//          Main Game Loop & Functions
// ============================================
void GameUpdate (BYTE *screen, int view)
{
    if (paused || player1.lives <= 0) {
        DrawStars (screen, view);
        return;
    }
    
    if (meteorCount < MaxMeteorCount){
        SpawnSmallMeteor();
    }
    
    UpdateObjects ();
    
    DrawObjects (screen, view);
}


void DrawObjects (BYTE *screen, int view)
{
    DrawStars (screen, view);
    
    Draw_player(screen, view);

    if (megaMeteorSpawned) {
       Draw_icosahedron(screen, view, megaMeteor.position, megaMeteor.scale, megaMeteor.angle, megaMeteor.axis);
    }
    
    int i = 0;
    for (; i < MaxNumMeteors; i++)
    {
        if (meteorsSpawned[i] == true)
            Draw_icosahedron(screen, view, meteors[i].position, meteors[i].scale, meteors[i].angle, meteors[i].axis);
    }
    
    for (i = 0; i < MaxBulletCount; i++)
    {
        if (bulletsSpawned[i] == true)
            Draw_bullet(screen, view, bullets[i]);
    }
}


void UpdateObjects ()
{
    UpdateAngle (player1.angle);
    
    if (megaMeteorSpawned) {
        UpdateMeteorPosition(megaMeteor, -1);
        UpdateAngle(megaMeteor.angle);
    }
    
    int i = 0;
    for (; i < MaxNumMeteors; i++)
    {
        if (meteorsSpawned[i] == true){
            UpdateMeteorPosition(meteors[i], i);
            UpdateAngle(meteors[i].angle);
        }
    }
    
    for (i = 0; i < MaxBulletCount; i++)
    {
        if (bulletsSpawned[i] == true)
            UpdateBullet(bullets[i], i);
    }
}


void UpdatePlayerScore ()
{
    player1.score += 100;

    if (player1.score == 500) {
        MaxMeteorCount++;
    }
    else if (player1.score == 1500) {
        MaxMeteorCount++;
        SpawnMegaMeteor();
    }
    else if (player1.score == 4000) {
        MaxMeteorCount++;
    }
    else if (player1.score == 8000) {
        SpawnMegaMeteor();
    }
    else if (player1.score >= 8000 && player1.score % 3000 == 0){
        SpawnMegaMeteor();
    }
}


void UpdatePlayerLives ()
{
    player1.lives--;
}




// ============================================
//   Bullet Spawn, Update & Destroy Functions
// ============================================
void SpawnBullet ()
{
    if (bulletCount == MaxBulletCount) { return; }
    if (!muted)
        PlaySoundEffect("Assets/Sounds/Laser.wav");
    
    int i = 0;
    while (bulletsSpawned[i] == true)
    {
        i++;
    }
    
    bulletsSpawned[i] = true;
    
    bullets[i].position.x = player1.position.x;
    bullets[i].position.y = player1.position.y + 20;
    bullets[i].position.z = player1.position.z;
    
    bullets[i].angle = 0;
    bullets[i].scale = 0.1;
    bullets[i].speed = bulletSpeed;
    
    bulletCount++;
}

void UpdateBullet (Bullet &bullet, int bulletNumber)
{
    bullet.position.y += bullet.speed;
    
    if (bullet.position.y >= (FRAME_HIGH /2 - 10) ) {
        RemoveBullet (bullet, bulletNumber );
        return;
    }
    
    for (int i = 0; i < MaxNumMeteors; i++)
    {
        if (megaMeteorSpawned && MeteorInRange(bullet, bulletNumber, -1)){
            if (!muted)
                PlaySoundEffect("Assets/Sounds/Explosion-2.wav");
            RemoveBullet (bullet, bulletNumber );
            DestroyMeteor (-1);
            UpdatePlayerScore ();
        }
        if (meteorsSpawned[i] == true && MeteorInRange(bullet, bulletNumber, i)){
            if (!muted)
                PlaySoundEffect("Assets/Sounds/Explosion-1.wav");
            RemoveBullet (bullet, bulletNumber );
            DestroyMeteor (i);
            UpdatePlayerScore ();
        }
    }
}

void RemoveBullet (Bullet &bullet, int bulletNumber)
{
    bulletCount--;
    bulletsSpawned[bulletNumber] = false;
}

bool MeteorInRange(Bullet &bullet, int bulletNumber, int meteorNumber)
{
    //-- if Megameteor
    if (meteorNumber == -1) {
        int c = sqrt(pow((bullet.position.x - megaMeteor.position.x), 2) +
                     pow((bullet.position.y - megaMeteor.position.y), 2));
        
        return (c < MeteorRangeLarge) ? true : false;
    }
    
    int c = sqrt(pow((bullet.position.x - meteors[meteorNumber].position.x), 2) +
                 pow((bullet.position.y - meteors[meteorNumber].position.y), 2));
    
    return (c < MeteorRangeSmall) ? true : false;
}


// ============================================
//   Meteor Spawn, Update & Destroy Functions
// ============================================
void SpawnSmallMeteor ()
{
    int i = rand() % MaxNumMeteors;
    
    while (meteorsSpawned[i] == true)
    {
        i = rand() % MaxNumMeteors;
    }
    
    meteorsSpawned[i] = true;
    
    //-- Alternate X positions
    if      (i == 0) {   meteors[i].position.x = 100;    }
    else if (i == 1) {   meteors[i].position.x = -100;   }
    else if (i == 2) {   meteors[i].position.x = 0;      }
    else if (i == 3) {   meteors[i].position.x = -200;   }
    else if (i == 4) {   meteors[i].position.x = 200;    }
    
    meteors[i].position.y = 250;
    meteors[i].position.z = 0;
    
    meteors[i].angle = 0;
    meteors[i].axis = rand() % 3;
    
    meteors[i].scale = MeteorScaleSmall;
    meteors[i].speed = rand() % 4 + -4;
    
    meteorCount++;
}

void SpawnSmallMeteor (int y)
{
    int i = rand() % MaxNumMeteors;
    
    while (meteorsSpawned[i] == true)
    {
        i = rand() % MaxNumMeteors;
    }
    
    meteorsSpawned[i] = true;
    
    //-- Alternate X positions
    if      (i == 0) {   meteors[i].position.x = 100;    }
    else if (i == 1) {   meteors[i].position.x = -100;   }
    else if (i == 2) {   meteors[i].position.x = 0;      }
    else if (i == 3) {   meteors[i].position.x = -200;   }
    else if (i == 4) {   meteors[i].position.x = 200;    }
    
    meteors[i].position.y = y;
    meteors[i].position.z = 0;
    
    meteors[i].angle = 0;
    meteors[i].axis = rand() % 3;
    
    meteors[i].scale = MeteorScaleSmall;
    meteors[i].speed = rand() % 4 + -4;
    
    meteorCount++;
}

void SpawnMegaMeteor ()
{
    megaMeteorSpawned = true;

    megaMeteor.position.x = 0;
    megaMeteor.position.y = 200;
    megaMeteor.position.z = 0;
    
    megaMeteor.angle = 0;
    megaMeteor.axis = rand() % 3;
    
    megaMeteor.scale = MeteorScaleLarge;
    megaMeteor.speed = -1;//rand() % 4 + -4;
}

void UpdateMeteorPosition(Meteor &meteor, int meteorNumber)
{
    meteor.position.y += meteor.speed;
    
    if (meteor.position.y <= -(FRAME_HIGH/2)) {
        if (!muted)
            PlaySoundEffect("Assets/Sounds/Hit.wav");
        player1.score -= 100;
        DestroyMeteor(meteorNumber);
        UpdatePlayerLives();
    }
}

void DestroyMeteor (int meteorNumber)
{
    if (meteorNumber == -1) {
        megaMeteorSpawned = false;
        
        if (meteorCount > 3) {
            SpawnSmallMeteor(megaMeteor.position.y);
            return;
        }
        
        SpawnSmallMeteor(megaMeteor.position.y);
        SpawnSmallMeteor(megaMeteor.position.y);
    }
    else{
        meteorsSpawned[meteorNumber] = false;
        meteorCount--;
    }
}



// ============================================
//            Additional Functions
// ============================================
void PlaySoundEffect(char * filename)
{
#ifdef _WIN32
    PlaySound(filename, NULL, SND_ASYNC | SND_FILENAME );
#else
    char command[80];
#ifdef __APPLE__
    sprintf(command, "afplay %s &", filename);
#else
    sprintf(command, "play %s &", filename);
#endif
    system(command);
#endif
}

void DrawStars (BYTE *screen, int view)
{
    for (int i = 0; i < NumStars; i++) {
        SetPixel ( screen, 3, view, stars[i].x, stars[i].y, 0xffffff );
        SetPixel ( screen, 3, view, stars[i].x+1, stars[i].y, 0xffffff );
        SetPixel ( screen, 3, view, stars[i].x, stars[i].y+1, 0xffffff );
        SetPixel ( screen, 3, view, stars[i].x+1, stars[i].y+1, 0xffffff );
    }
}



#endif // GAME_H