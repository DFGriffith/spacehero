#ifndef DRAWING_H
#define DRAWING_H

#include "datatypes.h"


// ==========================================
//           Pixel Drawing Functions
// ==========================================
void SetPixel ( BYTE* screen, int channels, int view, int x, int y, char red, char blue, char green)
{
    screen[ channels * (x + view + y * FRAME_WIDE) + 0 ] = red;
    screen[ channels * (x + view +y * FRAME_WIDE) + 1 ] = green;
    screen[ channels * (x + view +y * FRAME_WIDE) + 2 ] = blue;
}

void SetPixel ( BYTE* screen, int channels, int view, int x, int y, int color )
{
    screen[ channels * (x + view + y * FRAME_WIDE) + 0 ] = color >> 16 & 0xff;
    screen[ channels * (x + view + y * FRAME_WIDE) + 1 ] = color >> 8 & 0xff;
    screen[ channels * (x + view + y * FRAME_WIDE) + 2 ] = color & 0xff;
}


// ==========================================
//           Line Drawing Functions
// ==========================================
void DrawLine ( BYTE* screen, int view, int x1, int y1, int x2, int y2, int color )
{
    int r1 = color >> 16 & 0xff, g1 = color >> 8 & 0xff, b1 = color & 0xff;
    
    int dx = x2 - x1, dy = y2 - y1;
    int steps;
    
    if ( abs(dx) > abs(dy) )
        steps = abs(dx);
    else
        steps = abs(dy);
    
    double x_inc = dx / (double)steps, y_inc = dy / (double)steps;
    double x = x1, y = y1;
    
    SetPixel ( screen, 3, view, x, y, color );
    
    for ( int i = 0; i<steps; i++, x+=x_inc, y+=y_inc )
        SetPixel ( screen, 3, view, x, y, color );
}

// =========== Draw Gradient Line Function ===========
void DrawLine ( BYTE* screen, int view, int x1, int y1, int x2, int y2, int c1, int c2 )
{
    int r1 = c1 >> 16 & 0xff, g1 = c1 >> 8 & 0xff, b1 = c1 & 0xff;
    int r2 = c2 >> 16 & 0xff, g2 = c2 >> 8 & 0xff, b2 = c2 & 0xff;
    
    int dx = x2 - x1, dy = y2 - y1;
    int steps;
    
    if ( abs(dx) > abs(dy) )
        steps = abs(dx);
    else
        steps = abs(dy);
    
    double x_inc = dx / (double)steps;
    double y_inc = dy / (double)steps;
    double x = x1, y = y1;
    
    int length = steps;
    
    double rdiff = (r2 - r1) / (double)length;
    double gdiff = (g2 - g1) / (double)length;
    double bdiff = (b2 - b1) / (double)length;
    
    double rTemp = r1, gTemp = g1, bTemp = b1;
    
    SetPixel ( screen, 3, view, x, y, r1, g1, b1 );
    
    for ( int i=1; i<steps; i++, x+=x_inc, y+=y_inc )
    {
        rTemp += rdiff, gTemp += gdiff, bTemp += bdiff;
        SetPixel ( screen, 3, view, x, y, rTemp, gTemp, bTemp );
    }
}

// =========== Scan Line Function ===========
void ScanLine ( BYTE* screen, int view, int x1, int x2, int y, int c1, int c2 )
{
    int r1 = c1 >> 16 & 0xff, g1 = c1 >> 8 & 0xff, b1 = c1 & 0xff;
    int r2 = c2 >> 16 & 0xff, g2 = c2 >> 8 & 0xff, b2 = c2 & 0xff;
    
    int dx1 = x2 - x1, dx2 = x1 - x2;
    
    int leftX, rightX;
    
    if( x2 - x1 < 0 )
        leftX = x2, rightX = x1;
    else
        leftX = x1, rightX = x2;
    
    int length = rightX - leftX;
    
    double rdiff = (r2 - r1) / (double)(length);
    double gdiff = (g2 - g1) / (double)(length);
    double bdiff = (b2 - b1) / (double)(length);
    
    double rTemp = r1, gTemp = g1, bTemp = b1;
    
    int x = leftX;
    
    SetPixel ( screen, 3, view, x, y, rTemp, gTemp, bTemp );
    
    for ( int i = 1; i<length; i++, x++ )
    {
        rTemp += rdiff, gTemp += gdiff, bTemp += bdiff;
        SetPixel ( screen, 3, view, x, y, rTemp, gTemp, bTemp );
    }
}



// ==========================================
//             Triangle Drawing
// ==========================================
void SortTrianglePointsHightoLow ( Vector2 * vtx, int *order )
{
    // find the lowest Y
    if ( vtx[0].y < vtx[1].y ){
        if ( vtx[0].y < vtx[2].y )
            order[2] = 0;
        else
            order[2] = 2;
    }
    else{
        if ( vtx[1].y < vtx[2].y )
            order[2] = 1;
        else
            order[2] = 2;
    }
    // find the highest Y
    if ( vtx[0].y > vtx[1].y ){
        if ( vtx[0].y > vtx[2].y )
            order[0] = 0;
        else
            order[0] = 2;
    }
    else{
        if ( vtx[1].y > vtx[2].y )
            order[0] = 1;
        else
            order[0] = 2;
    }
    // set middle
    order[1] = 3 - (order[0] + order[2]);
}

float CrossProduct ( Vector2 & v1, Vector2 & v2 )
{
    return (v1.x * v2.y) - (v1.y * v2.x);
}

void TriangleFindLeft ( Vector2 * vtx, int *order )
{
    if ( vtx[order[1]].x > vtx[order[2]].x )
    {
        int holder = order[1];
        order[1] = order[2];
        order[2] = holder;
    }
}


// =========== Draw Standard Triangle ===========
void Triangle ( BYTE* screen, int view, Vector2 vec1, Vector2 vec2, Vector2 vec3, int color )
{
    //-- Sort the triangle vertices and colors
    int order[] = { 0, 1, 2 };
    Vector2 vtx[] = { vec1, vec2, vec3 };
    
    SortTrianglePointsHightoLow ( vtx, order );
    
    //-- Remove vertices for easier manipulation
    int x0 = vtx[order[0]].x, y0 = vtx[order[0]].y;
    int x1 = vtx[order[1]].x, y1 = vtx[order[1]].y;
    int x2 = vtx[order[2]].x, y2 = vtx[order[2]].y;
    
    double mL = (x1 - x0) / (double)(y0 - y1);
    double mR = (x2 - x0) / (double)(y0 - y2);
    double xL = x0, xR = x0;
    
    //-- Find furthest point
    int hL = y0 - y1, hR = y0 - y2;
    int high = (hL > hR) ? hL : hR;
    
    int Ledge[FRAME_HIGH], Redge[FRAME_HIGH]; //- edge arrays
    
    //-- If the triangle has a flat top build it upside down
    if ( vtx[order[0]].y == vtx[order[1]].y || vtx[order[0]].y == vtx[order[2]].y )
    {
        int i = y2, stop = y2 + high;
        xL = x2, xR = x2;
        
        mL = (x1 - x2) / (double)(y1 - y2);
        mR = (x0 - x2) / (double)(y0 - y2);
        
        for ( ; i < stop; i++ )
        {
            Ledge[i] = xL, Redge[i] = xR;
            xL += mL, xR += mR;
            
            DrawLine(screen, view, Ledge[i], i, Redge[i], i, color);
        }
    }
    //-- Else build triangle normally
    else
    {
        int i = y0, stop = y0 - high;
        
        for ( ; i > stop; i-- )
        {
            Ledge[i] = xL, Redge[i] = xR;
            
            if (i == y0-hL)
                mL = (x1 - x2) / (double)(y2 - y1);
            
            if (i == y0-hR)
                mR = (x2 - x1) / (double)(y1 - y2);
            
            xL += mL, xR += mR;
            
            DrawLine(screen, view, Ledge[i], i, Redge[i], i, color);
        }
    }
}


// =========== Draw Shaded Triangle ===========
void ShadedTriangle ( BYTE* screen, int view, Vector2 vec1, Vector2 vec2, Vector2 vec3, int cl0, int cl1, int cl2 )
{
    //-- Sort the triangle vertices and colors
    int order[] = { 0, 1, 2 };
    int colors[] = { cl1, cl2, cl0 };
    Vector2 vtx[] = { vec1, vec2, vec3 };
    
    SortTrianglePointsHightoLow ( vtx, order );
    
    //-- Remove colors and vertice for easier manipulation
    int c0 = colors[ order[0] ],  c1 = colors[ order[1] ],  c2 = colors[ order[2] ];
    int r0 = c0 >> 16 & 0xff,   b0 = c0 >> 8 & 0xff,    g0 = c0 & 0xff;
    int r1 = c1 >> 16 & 0xff,   b1 = c1 >> 8 & 0xff,    g1 = c1 & 0xff;
    int r2 = c2 >> 16 & 0xff,   b2 = c2 >> 8 & 0xff,    g2 = c2 & 0xff;
    
    int x0 = vtx[ order[0] ].x, y0 = vtx[ order[0] ].y;
    int x1 = vtx[ order[1] ].x, y1 = vtx[ order[1] ].y;
    int x2 = vtx[ order[2] ].x, y2 = vtx[ order[2] ].y;
    
    //-- Identify Left and Right vetices
    int leftX, leftY, rightX, rightY, leftR, leftG, leftB, rightR, rightG, rightB;
    if ( x2 < x1 ){
        leftX = x2, leftY = y2;
        rightX = x1, rightY = y1;
        leftR = r2, leftG = g2, leftB = b2;
        rightR = r1, rightG = g1, rightB = b1;
    }
    else{
        leftX = x1, leftY = y1;
        rightX = x2, rightY = y2;
        leftR = r1, leftG = g1, leftB = b1;
        rightR = r2, rightG = g2, rightB = b2;
    }
    
    double mL = (leftX - x0) / (double)(y0 - leftY);
    double mR = (rightX - x0) / (double)(y0 - rightY);
    double xL = x0, xR = x0;
    
    //-- Find furthest point
    int hL = y0 - leftY, hR = y0 - rightY;
    int high = ( hL > hR ) ? hL : hR;
    
    //-- Calculate color differences
    double rdiff = (leftR - r0) / (double)(hL);
    double gdiff = (leftG - g0) / (double)(hL);
    double bdiff = (leftB - b0) / (double)(hL);
    double rdiff2 = (rightR - r0) / (double)(hR);
    double gdiff2 = (rightG - g0) / (double)(hR);
    double bdiff2 = (rightB - b0) / (double)(hR);
    
    double rTemp = r0, gTemp = g0, bTemp = b0;
    double rTemp2 = r0, gTemp2 = g0, bTemp2 = b0;
    
    EDGE_LIST list;  // Edge list
    int colorR = c0, colorL = c0;
    
    //-- If the triangle has a flat top build it upside down
    if ( vtx[order[0]].y == vtx[order[1]].y || vtx[order[0]].y == vtx[order[2]].y )
    {
        int i = y2-1, stop = y2 + high;
        xL = x2, xR = x2;
        
        if ( x0 < x1 )
        {
            leftX = x0, leftY = y0;
            rightX = x1, rightY = y1;
            leftR = r0, leftG = g0, leftB = b0;
            rightR = r1, rightG = g1, rightB = b1;
        }
        else
        {
            leftX = x1, leftY = y1;
            rightX = x0, rightY = y0;
            leftR = r1, leftG = g1, leftB = b1;
            rightR = r0, rightG = g0, rightB = b0;
        }
        
        mL = (leftX - x2) / (double)(leftY - y2);
        mR = (rightX - x2) / (double)(rightY - y2);
        
        //-- Calculate color differences
        rdiff = (leftR - r2) / (double)(high);
        gdiff = (leftG - g2) / (double)(high);
        bdiff = (leftB - b2) / (double)(high);
        rdiff2 = (rightR - r2) / (double)(high);
        gdiff2 = (rightG - g2) / (double)(high);
        bdiff2 = (rightB - b2) / (double)(high);
        
        rTemp = r2, gTemp = g2, bTemp = b2;
        rTemp2 = r2, gTemp2 = g2, bTemp2 = b2;
        
        colorR = c2, colorL = c2;
        
        for ( ; i < stop; i++ )
        {
            list[i].xL = xL+1, list[i].xR = xR+1;
            list[i].cL = colorL, list[i].cR = colorR;
            
            DrawLine( screen, view, list[i].xL, i, list[i].xR, i, list[i].cL, list[i].cR );
            
            xL += mL;
            xR += mR;
            
            rTemp += rdiff, gTemp += gdiff, bTemp += bdiff;
            rTemp2 += rdiff2, gTemp2 += gdiff2, bTemp2 += bdiff2;
            colorL = (((int)rTemp & 0xff) << 16) + (((int)gTemp & 0xff) << 8) + ((int)bTemp & 0xff);
            colorR = (((int)rTemp2 & 0xff) << 16) + (((int)gTemp2 & 0xff) << 8) + ((int)bTemp2 & 0xff);
        }
    }
    //-- build normal triangle
    else
    {
        int i = y0, stop = y0 - high;
        
        int cl = (((int)leftR & 0xff) << 16) + (((int)leftG & 0xff) << 8) + ((int)leftB & 0xff);
        int cr = (((int)rightR & 0xff) << 16) + (((int)rightG & 0xff) << 8) + ((int)rightB & 0xff);
        
        for ( ; i > stop; i-- )
        {
            list[i].xL = xL-1,     list[i].xR = xR-1;
            list[i].cL = colorL, list[i].cR = colorR;
            
            if ( i == leftY ){
                mL = (rightX - leftX) / (double)(leftY - rightY);
                
                rdiff = (rightR - leftR) / (double)(leftY - rightY);
                gdiff = (rightG - leftG) / (double)(leftY - rightY);
                bdiff = (rightB - leftB) / (double)(leftY - rightY);
            }
            if ( i == rightY ){
                mR = (leftX - rightX) / (double)(rightY - leftY);
                
                rdiff2 = (leftR - rightR) / (double)(rightY - leftY);
                gdiff2 = (leftG - rightG) / (double)(rightY - leftY);
                bdiff2 = (leftB - rightB) / (double)(rightY - leftY);
            }
            
            DrawLine ( screen, view, list[i].xL, i, list[i].xR, i, list[i].cR, list[i].cL );
            
            xL += mR;
            xR += mL;
            
            rTemp += rdiff, gTemp += gdiff, bTemp += bdiff;
            rTemp2 += rdiff2, gTemp2 += gdiff2, bTemp2 += bdiff2;
            
            colorL = (((int)rTemp & 0xff) << 16) + (((int)gTemp & 0xff) << 8) + ((int)bTemp & 0xff);
            colorR = (((int)rTemp2 & 0xff) << 16) + (((int)gTemp2 & 0xff) << 8) + ((int)bTemp2 & 0xff);
        }
    }
}




// ==========================================
//         Polygon Drawing Functions
// ==========================================
// =========== Convex Polygon Drawing Function (Single Color) ===========
void ConvexTriangulate (BYTE *screen, int view, int vcount, Vector2* varray, int color)
{
    int i;
    for (i = 1; i < vcount-1; i++)
    {
        Triangle ( screen, view, varray[0], varray[i], varray[i+1], color );
    }
    
    for (i = 0; i < vcount; i++) {
        DrawLine ( screen, view, varray[i].x, varray[i].y,
                  varray[(i+1) % vcount].x, varray[(i+1) % vcount].y, 0xffffff);
    }
}

// =========== Convex Polygon Drawing Function (Multi Color) ===========
void ConvexTriangulate (BYTE *screen, int view,  int vcount, Vector2* varray, int* colors)
{
    int i = 1;
    for (; i < vcount-1; i++)
    {
        ShadedTriangle ( screen, view, varray[0], varray[i], varray[i+1], colors[0], colors[i], colors[i+1] );
    }
    
    for (i = 0; i < vcount; i++) {
        DrawLine ( screen, view, varray[i].x, varray[i].y,
                  varray[(i+1) % vcount].x, varray[(i+1) % vcount].y, 0xffffff);
    }
}




// ==========================================
//             Line Clipping
// ==========================================
// =========== Clip Line Function (Liang-Barsky) ===========
bool ClipLine (double edgeLeft, double edgeRight, double edgeBottom, double edgeTop,
               double x0, double y0, double x1, double y1,
               double &x0clip, double &y0clip, double &x1clip, double &y1clip)
{
    double t0 = 0.0, t1 = 1.0;
    double xdelta = x1 -x0, ydelta = y1 -y0;
    double p,q,r;
    
    for(int edge=0; edge<4; edge++)
    {
        if (edge==0) {  p = -xdelta;    q = -(edgeLeft-x0);  }
        if (edge==1) {  p = xdelta;     q =  (edgeRight-x0); }
        if (edge==2) {  p = -ydelta;    q = -(edgeBottom-y0);}
        if (edge==3) {  p = ydelta;     q =  (edgeTop-y0);   }
        
        r = q/p;
        
        if (p==0 && q<0)
            return false;     // Return draw line flase (parallel line outside)
        
        if(p<0){
            if(r>t1)
                return false; // Return draw line false
            else if(r>t0)
                t0 = r;       // Clip line
        }
        else if (p>0){
            if (r<t0)
                return false; // Return draw line false
            else if (r<t1)
                t1 = r;       // Clip line
        }
    }
    
    x0clip = x0 + t0 * xdelta, y0clip = y0 + t0 * ydelta;
    x1clip = x0 + t1 * xdelta, y1clip = y0 + t1 * ydelta;
    
    return true;  // Return draw line true
}

// =========== Clip & Draw Line Function (Liang-Barsky) ===========
void ClipDrawLine ( BYTE* screen, int view, double edgeLeft, double edgeRight, double edgeBottom, double edgeTop,
               double x0, double y0, double x1, double y1, int c1, int c2 )
{
    double t0 = 0.0, t1 = 1.0;
    double xdelta = x1 -x0, ydelta = y1 -y0;
    double p, q, r;
    double newX0, newY0, newX1, newY1;
    
    for (int edge = 0; edge < 4; edge++)
    {
        if (edge == 0) {  p = -xdelta;    q = -(edgeLeft-x0);  }
        if (edge == 1) {  p = xdelta;     q =  (edgeRight-x0); }
        if (edge == 2) {  p = -ydelta;    q = -(edgeBottom-y0);}
        if (edge == 3) {  p = ydelta;     q =  (edgeTop-y0);   }
        
        r = q/p;
        
        if (p == 0 && q < 0)
            return;           // Dont draw line (parallel line outside)
        
        if(p < 0)
        {
            if(r > t1)
                return;       // Dont draw line
            else if(r > t0)
                t0 = r;       // Clip line
        }
        else if (p > 0)
        {
            if (r < t0)
                return;       // Dont draw line
            else if (r < t1)
                t1 = r;       // Clip line
        }
    }
    
    newX0 = x0 + t0 * xdelta,   newY0 = y0 + t0 * ydelta;
    newX1 = x0 + t1 * xdelta,   newY1 = y0 + t1 * ydelta;
    
    DrawLine (screen, view, newX0, newY0, newX1, newY1, c1, c2);
}



// ==========================================
//        Polygon Clipping Functions
// ==========================================
// =========== Frame Bounds Checking Function ===========
int CheckBounds(Vector2 vec1, Vector2 vec2)
{
    if (vec1.x < 0 || vec1.x > FRAME_WIDE || vec1.y < 0 || vec1.y > FRAME_HIGH) {
        if (vec2.x < 0 || vec2.x > FRAME_WIDE || vec2.y < 0 || vec1.y > FRAME_HIGH) {
            return 0;
        }
        else{
            return 1;
        }
    }
    else if (vec2.x < 0 || vec2.x > FRAME_WIDE || vec2.y < 0 || vec1.y > FRAME_HIGH) {
        return 3;
    }
    else{
        return 2;
    }
}

// =========== Clip Polygon (Multi-Color) ===========
void ConvexClipPolygon (BYTE* screen, int view, Vector2* varray, int vcount, int* colors)
{
    double x0clip, y0clip, x1clip, y1clip;
    
    Vector2 varrayNew[100];
    int NAindex = 0, boundsCase = 0;
    
    for ( int i = 0; i < vcount; i++)
    {
        if (i == vcount-1) {
            boundsCase = CheckBounds(varray[i], varray[0]);
            
            switch (boundsCase) {
                case 0:
                {
                    break;
                }
                case 1:
                {
                    ClipLine ( 0, FRAME_WIDE, 0, FRAME_HIGH,
                              varray[i].x, varray[i].y, varray[0].x, varray[0].y,
                              x0clip, y0clip, x1clip, y1clip);
                    varrayNew[NAindex].x = x0clip, varrayNew[NAindex++].y = y0clip;
                    varrayNew[NAindex].x = varray[0].x, varrayNew[NAindex++].y = varray[0].y;
                    break;
                }
                case 2:
                {
                    varrayNew[NAindex].x = varray[0].x, varrayNew[NAindex++].y = varray[0].y;
                    break;
                }
                case 3:
                {
                    ClipLine ( 0, FRAME_WIDE, 0, FRAME_HIGH,
                              varray[i].x, varray[i].y, varray[0].x, varray[0].y,
                              x0clip, y0clip, x1clip, y1clip);
                    varrayNew[NAindex].x = x1clip, varrayNew[NAindex++].y = y1clip;
                    break;
                }
            }
        }
        else{
            boundsCase = CheckBounds(varray[i], varray[i+1]);
            
            switch (boundsCase) {
                case 0:
                {
                    break;
                }
                case 1:
                {
                    ClipLine ( 0, FRAME_WIDE, 0, FRAME_HIGH,
                              varray[i].x, varray[i].y, varray[i+1].x, varray[i+1].y,
                              x0clip, y0clip, x1clip, y1clip);
                    varrayNew[NAindex].x = x0clip, varrayNew[NAindex++].y = y0clip;
                    varrayNew[NAindex].x = varray[i+1].x, varrayNew[NAindex++].y = varray[i+1].y;
                    break;
                }
                case 2:
                {
                    varrayNew[NAindex].x = varray[i+1].x, varrayNew[NAindex++].y = varray[i+1].y;
                    break;
                }
                case 3:
                {
                    ClipLine ( 0, FRAME_WIDE, 0, FRAME_HIGH,
                              varray[i].x, varray[i].y, varray[i+1].x, varray[i+1].y,
                              x0clip, y0clip, x1clip, y1clip);
                    varrayNew[NAindex].x = x1clip, varrayNew[NAindex++].y = y1clip;
                    break;
                }
            }
        }
    }
    
    ConvexTriangulate(screen, view, NAindex, varrayNew, colors);
}

// =========== Clipped Polygon (Single-Color) ===========
void ConvexClipPolygon (BYTE* screen, int view, Vector2* varray, int vcount, int color)
{
    double x0clip, y0clip, x1clip, y1clip;
    
    Vector2 varrayNew[100];
    int NAindex = 0, boundsCase = 0;
    
    for ( int i = 0; i < vcount; i++)
    {
        if (i == vcount-1) {
            boundsCase = CheckBounds(varray[i], varray[0]);
            
            switch (boundsCase) {
                case 0:
                {
                    break;
                }
                case 1:
                {
                    ClipLine ( 0, FRAME_WIDE, 0, FRAME_HIGH,
                              varray[i].x, varray[i].y, varray[0].x, varray[0].y,
                              x0clip, y0clip, x1clip, y1clip);
                    varrayNew[NAindex].x = x0clip, varrayNew[NAindex++].y = y0clip;
                    varrayNew[NAindex].x = varray[0].x, varrayNew[NAindex++].y = varray[0].y;
                    break;
                }
                case 2:
                {
                    varrayNew[NAindex].x = varray[0].x, varrayNew[NAindex++].y = varray[0].y;
                    break;
                }
                case 3:
                {
                    ClipLine ( 0, FRAME_WIDE, 0, FRAME_HIGH,
                              varray[i].x, varray[i].y, varray[0].x, varray[0].y,
                              x0clip, y0clip, x1clip, y1clip);
                    varrayNew[NAindex].x = x1clip, varrayNew[NAindex++].y = y1clip;
                    break;
                }
            }
        }
        else{
            boundsCase = CheckBounds(varray[i], varray[i+1]);
            
            switch (boundsCase) {
                case 0:
                {
                    break;
                }
                case 1:
                {
                    ClipLine ( 0, FRAME_WIDE, 0, FRAME_HIGH,
                              varray[i].x, varray[i].y, varray[i+1].x, varray[i+1].y,
                              x0clip, y0clip, x1clip, y1clip);
                    varrayNew[NAindex].x = x0clip, varrayNew[NAindex++].y = y0clip;
                    varrayNew[NAindex].x = varray[i+1].x, varrayNew[NAindex++].y = varray[i+1].y;
                    break;
                }
                case 2:
                {
                    varrayNew[NAindex].x = varray[i+1].x, varrayNew[NAindex++].y = varray[i+1].y;
                    break;
                }
                case 3:
                {
                    ClipLine ( 0, FRAME_WIDE, 0, FRAME_HIGH,
                              varray[i].x, varray[i].y, varray[i+1].x, varray[i+1].y,
                              x0clip, y0clip, x1clip, y1clip);
                    varrayNew[NAindex].x = x1clip, varrayNew[NAindex++].y = y1clip;
                    break;
                }
            }
        }
    }
    
    ConvexTriangulate(screen, view, NAindex, varrayNew, color);
}







#endif // DRAWING_H