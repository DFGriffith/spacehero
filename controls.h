#ifndef TRANSFORMATION_H
#define TRANSFORMATION_H

#include "data.h"

// ==============================================
//                Player Controls
// ==============================================
void UpdatePlayerPosition(int move)
{
    player1.position.x += move;
    
    if (player1.position.x <= -(FRAME_WIDE / 2) + 50) {
        player1.position.x = -(FRAME_WIDE / 2) + 50;
    }
    if (player1.position.x >= (FRAME_WIDE / 2) - 50) {
        player1.position.x = (FRAME_WIDE / 2) - 50;
    }
    
}

void UpdatePlayerPositionUD(int move)
{
    player1.position.y += move;
    
    if (player1.position.y <= -(FRAME_HIGH / 2) + 50) {
        player1.position.y = -220;
    }
    if (player1.position.y >= (FRAME_HIGH / 2) - 220) {
        player1.position.y = (FRAME_HIGH / 2) - 220;
    }
    
}


// ==============================================
//                 Game Controls
// ==============================================
void GameMute()
{
    if (muted)
        muted = false;
    else
        muted = true;
}

void GamePause ()
{
    if (paused)
        paused = false;
    else
        paused = true;
}

void GameReset ()
{
    MaxBulletCount = 3;
    bulletSpeed = 10;
    bulletCount = 0;
    
    for (int i = 0; i < MaxNumBullets; i++)
    {
        bulletsSpawned[i] = false;
    }
    for (int i = 0; i < MaxNumMeteors; i++)
    {
        meteorsSpawned[i] = false;
    }

    meteorCount = 0;
    MaxMeteorCount = 1;
    
    megaMeteorSpawned = false;
    
    SetupPlayer1();
    
    paused = false;
}

#endif // TRANSFORMATION_H